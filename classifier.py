#-*-coding:utf-8-*-

import utils 
import pandas as pds
import numpy as npy
import sklearn as skl
import sklearn.preprocessing as sklp
import sklearn.model_selection as sklm
import time
import matplotlib.pylab as pyl
from matplotlib.font_manager import FontProperties
import platform
def read_data():
    data = pds.read_csv("./iris.csv") #读取数据
    feature =  data.describe() #获取数据信息
    idata = data.T #数据转置，方便获取行和列
    odata = idata[0:4] #获取特征值
    labels = idata[4:].T #获取特征值对应的标签
    idata = odata.T #还原为读取出来的格式，方便处理
    return idata,feature,labels #返回特征值，数据信息，特征标签，

def getfont():
    sysstr = platform.system() #判断操作系统
    if sysstr == "Windows": #windows操作系统
        font_p = r"C:/Windows/Fonts/simhei.ttf"
    else: #买不起苹果，所以其他都是Linux字体路径
        font_p = r"/usr/share/fonts/truetype/wqy/wqy-microhei.ttc"
    font = FontProperties(fname=font_p,size=14)
    return font

def stdprocessdata(data,labels): #数据标准化预处理
    stddata = utils.mstandard(data) # 等价于 sklp.scale(data) ，标准化
    train_x,test_x,train_y,test_y = sklm.train_test_split(stddata,labels,test_size=0.2) #数据切分成训练数据和测试数据
    #print(npy.array(test_y))
    return npy.array(train_x),npy.array(test_x),npy.array(train_y),npy.array(test_y),npy.array(stddata) #转为数组后返回，方便处理

def nomprocessdata(data,labels): #数据归一化预处理
    nomdata = utils.mnormal(data) #等价于 sklp.MinMaxScaler().fit_transform(idata)，数据归一化
    train_x,test_x,train_y,test_y = sklm.train_test_split(nomdata,labels,test_size=0.2)#数据切分成训练数据和测试数据
    #print(npy.array(test_y))
    return npy.array(train_x),npy.array(test_x),npy.array(train_y),npy.array(test_y),npy.array(nomdata)#转为数组后返回，方便处理


def knn(train_x,train_y,test_x_single,dismethod = utils.eudist,k=5):#knn分类
    """
    特殊参数说明：
    dismethod：计算距离的方法名，在utils模块中，实现了欧式距离的计算和余弦相似度的计算，均可用在这里计算距离，只是效果不同
    """
    rowsize = train_x.shape[0] #获取训练数据的行数
    dis = dismethod(train_x,test_x_single) #计算测试数据与训练数据中每一个数据的距离
    disT = dis.T #将结果转置一下，方便排序
    #print(disT)
    sortdisidx = disT.argsort() #对距离进行排序，如果采用欧式距离计算，请按照从小到大排序，如果采用余弦相似度计算，因为1代表两个向量的夹角为0，所以应从大到小排序,sortdisidx = (-disT).argsort()
    votelabdic = {} #统计在数量k内各个分类存在的数量，作为最终分类的依据
    for i in range(k):
        votelab = train_y[sortdisidx[i]][0]
        votelabdic[votelab] = votelabdic.get(votelab,0)+1
    
    sortdic = sorted(votelabdic.items(),key=lambda item:item[1],reverse=True)#对统计结果排序，从大到小排，最后直接返回第一个就是类别
    print("knn out:",sortdic[0][0])
    print("##################################################")
    return sortdic[0][0]


def beyestrain(train_x,train_y):#贝叶斯训练函数
    trainrowsize = train_x.shape[0] #行数
    traincolsize = train_x.shape[1] #列数
    train_x = train_x*npy.tile(10,(trainrowsize,traincolsize)) #将各特征都扩大十倍，方便统计，原理请查看文档：扩10概述
    dis = npy.arange(0,11) #在0到11之间生成连续整数，代表范围，生成[0,1,...,9,10],分别代表:0->[0,1),1->[1,2),...,9->[9,10),10->[10,无穷大)  原理请查看文档：扩10概述
    allcls = set(list(train_y.T[0])) #获取所有类别
    alllb = list(train_y.T[0]) #获取所有标签
    lbdict = dict() #类别权重字典
    for lb in allcls: #计算各类别权重
        lbdict[lb] = list(train_y).count(lb)/len(list(train_y))
    lbirisdict = dict() #按照{类别1:[特征向量1，特征向量2，...，特征向量n]，类别2:[特征向量1，特征向量2，...，特征向量n]，，...，类别n:[特征向量1，特征向量2，...，特征向量n]}的方式存储,方便计算
    libweightdic = dict()
    for i in range(trainrowsize):#分类训练数据填充到lbirisdict中
        if lbirisdict.get(alllb[i],0) == 0:
            lbirisdict[alllb[i]] = []
        lbirisdict[alllb[i]].append(train_x[i])
    for key in lbirisdict: #计算每个类别的每一个特征在dis每个范围内的权重，存储在libweightdic中，每个类别的特征向量有四个特征，每个特征在每段上的权重为[w1,w2,...,w11]，
                            #存储方式为：{类别1：[[w1,w2,...,w11],[w1,w2,...,w11],[w1,w2,...,w11],[w1,w2,...,w11]]，...，类别n：[[w1,w2,...,w11],[w1,w2,...,w11],[w1,w2,...,w11],[w1,w2,...,w11]]}，其中wn代表该特征向量在dis[n]段上的权重
        #print(npy.array(lbirisdict[key]).T.astype(int))
        irisarr = list(npy.array(lbirisdict[key]).T.astype(int)) #将扩大十倍的训练数据进行转置并对各个特征向下取整
        for iris in list(irisarr):
            irisfeat = list(iris)
            di = []
            for fdx in dis:
                di.append(irisfeat.count(fdx)/len(irisfeat)) #统计某个段位对某个类别的某个特征的权重，以某个类别的第一个特征为例，公式：权重 = 该类别的所有特征向量第一个特征在该范围内的数量/所有类别的所有特征向量第一个特征的总数
            if libweightdic.get(key,0) == 0:
                libweightdic[key] = []
            libweightdic[key].append(di)
    print("训练完成")
    return libweightdic,lbdict #返回每个类别的每一个特征在dis每个范围内的权重和该类别的权重

def beyes(libweightdic,lbdict,test_x_single):#beyes分类
    print(test_x_single.shape[0])
    testcolsize = test_x_single.shape[0] #计算该测试数据的特征向量有多少个特征
    test_x_single = test_x_single*npy.tile(10,(1,testcolsize)) #将该特征向量的每个特征扩大10倍
    t_x = list(test_x_single.astype(int))#将扩大十倍的测试数据各特征值取整并转为list，方便统计
    scores = dict() #测试数据是某个类别的概率，存储方式：{类别1：概率，类别2：概率,...，类别n：概率}
    for key in libweightdic:#计算测试数据是某个类别的概率
        p = 1
        for idx in range(0,testcolsize):
            #print(t_x[0][idx])
            p = p*(libweightdic[key][idx][t_x[0][idx]])
        
        scores[key] = p*lbdict[key]
    sortres = sorted(scores.items(),key=lambda item:item[1],reverse = True)  #对计算得到的类别概率从大到小排序，第一个就是所属分类
    print("beyes out:",sortres[0][0])
    print("##################################################")
    return sortres[0][0]   
    


def testknn():#knn测试方法
    data,feature,labels = read_data() #读取数据
    train_x,test_x,train_y,test_y,stddata = stdprocessdata(data,labels)  #数据标准化，切分数据
    trowsize = test_x.shape[0] #获取测试数据的的总数
    start_t = time.time()
    truecount = 0
    #print(stddata)
    for i in range(trowsize): #将测试数据一个一个丢进knn测试
        print("##################################################")
        print("knn in:",test_y[i][0])
        if test_y[i][0] ==knn(train_x,train_y,test_x[i]): #判断knn得到的结果和预期是否一致，与预期一致，则在正确数量上加1
            truecount += 1
    end_t = time.time()
    accuracy = truecount/trowsize #预测正确的数量除以总数，得到准确率
    mtime = end_t-start_t
    print("=================================================")
    print("knn时间消耗为：",mtime)
    print("knn准确率为：",accuracy)
    return mtime,accuracy

def testbeyes():#beyes测试方法
    data,feature,labels = read_data()
    train_x,test_x,train_y,test_y,nomdata = nomprocessdata(data,labels)#数据归一化，切分数据
    trowsize = test_x.shape[0]
    start_t = time.time()
    truecount = 0
    #print(stddata)
    libweightdic,lbdict = beyestrain(train_x,train_y) #对beyes进行训练
    for i in range(trowsize):#将测试数据一个一个丢进beyes测试
        print("##################################################")
        print("beyes in",test_y[i][0])
        if test_y[i][0] ==beyes(libweightdic,lbdict,npy.array(test_x[i])):
            truecount += 1
    end_t = time.time()
    accuracy = truecount/trowsize#预测正确的数量除以总数，得到准确率
    mtime = end_t-start_t
    print("=================================================")
    print("beyes时间消耗为：",mtime)
    print("beyes准确率为：",accuracy)
    return mtime,accuracy
    
def main():
    tk = [] #knn时间列表
    tb = [] #beyes时间列表
    ak = [] #knn准确率列表
    ab = [] #beyes准确率列表
    for i in range(0,200): #测试200次，统计结果
        mtime,accuracy = testbeyes()
        tk.append(mtime)
        ak.append(accuracy)
        mtime,accuracy =testknn()
        tb.append(mtime)
        ab.append(accuracy)
    print("-----------------------------数据统计start--------------------------")
    print("knn时间消耗平均值为：",npy.array(tk).mean(0))
    print("knn时间消耗方差为：",npy.array(tk).std(0))
    print("knn准确率平均值为：",npy.array(ak).mean(0))
    print("knn准确率方差为：",npy.array(ak).std(0))
    print("beyes时间消耗平均值为：",npy.array(tb).mean(0))
    print("beyes时间消耗方差为：",npy.array(tb).std(0))
    print("beyes准确率平均值为：",npy.array(ab).mean(0))
    print("beyes准确率方差为：",npy.array(ab).std(0))
    print("-----------------------------数据统计end--------------------------")
    pyl.subplots_adjust(hspace=0.97,top = 0.92,wspace=0.32)
    pyl.subplot(4,1,1)
    pyl.title("时间散点图",fontproperties = getfont())
    pyl.xlabel("knn时间",fontproperties = getfont())
    pyl.ylabel("beyes时间",fontproperties = getfont())
    pyl.plot(tk,tb,"or")
    pyl.subplot(4,1,2)
    pyl.title("准确度散点图",fontproperties = getfont())
    pyl.xlabel("knn准确率",fontproperties = getfont())
    pyl.ylabel("beyes准确率",fontproperties = getfont())
    pyl.plot(ak,ab,"or")
    
    r = npy.arange(0,1.1,0.1)
    print(r)
    pyl.subplot(4,2,5)
    pyl.title("knn时间直方图",fontproperties = getfont())
    pyl.xlabel("knn 时间",fontproperties = getfont())
    pyl.ylabel("数量",fontproperties = getfont())
    pyl.hist(tk,r)
    pyl.subplot(4,2,6)
    pyl.title("knn准确率直方图",fontproperties = getfont())
    pyl.xlabel("knn 准确率",fontproperties = getfont())
    pyl.ylabel("数量",fontproperties = getfont())
    pyl.hist(ak,r)
    pyl.subplot(4,2,7)
    pyl.title("beyes时间直方图",fontproperties = getfont())
    pyl.xlabel("beyes 时间",fontproperties = getfont())
    pyl.ylabel("数量",fontproperties = getfont())
    pyl.hist(tb,r)
    pyl.subplot(4,2,8)
    pyl.title("beyes准确率直方图",fontproperties = getfont())
    pyl.xlabel("beyes准确率",fontproperties = getfont())
    pyl.ylabel("数量",fontproperties = getfont())
    pyl.hist(ab,r)
    pyl.show()

    
main()
